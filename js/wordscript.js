/*
 * To get the Random Word with Meaning on Popdown
 */

var random = Math.floor(Math.random() * 3410) + 1;

function loadJSON(callback) {
    var xobj = new XMLHttpRequest();
    xobj.overrideMimeType("application/json");
    xobj.open('GET', 'worddict.json', true);
    xobj.onreadystatechange = function() {
        if (xobj.readyState == 4 && xobj.status == "200") {
            callback(xobj.responseText);
        }
    };
xobj.send(null);
}

var actual_json = [];
loadJSON(function(response) {
    actual_json = JSON.parse(response);
    for (var i in actual_json) {
        random = random-1;
        if (random == 1){
            document.getElementsByClassName('card-title')[0].innerHTML=i.toUpperCase();
            document.getElementsByClassName('meaning')[0].innerHTML=actual_json[i][0];
        }
    }
});
