function closePopup() {
  if (SAFARI) {
    for (var a = safari.extension.toolbarItems, b = 0; b < a.length; b++)
      if ("clipper" == a[b].identifier && a[b].popover) {
        a[b].popover.hide();
        break
      }
  } else
      window.close();
}


