var data = {};

// LOAD SAT FILE
var xhr = new XMLHttpRequest();
xhr.onreadystatechange = function(){
    if (xhr.readyState === 4){
        data = JSON.parse(xhr.response);
    }
}
xhr.open("GET", chrome.extension.getURL('word_lists/dictionary.json'), false);
xhr.send();


var BLACK_LIST = ['search', 'view=cm', 'compose', 'mail', 'login', 'email'];

var MAX_CHANGES_PER_PAGE = 10;

function uuid() {
    var uuid = "", i, random;
    for (i = 0; i < 32; i++) {
        random = Math.random() * 16 | 0;

        if (i == 8 || i == 12 || i == 16 || i == 20) {
            uuid += "-"
        }
        uuid += (i == 12 ? 4 : (i == 16 ? (random & 3 | 8) : random)).toString(16);
    }
    return uuid;
}

//data = {"nuestro":["nuestro","nuestro"]};

function replaceText(el){

    for (var e in data){
        if (data[e].hasOwnProperty){

            // Create a NodeIterator to find <img> tags
            var walker = document.createNodeIterator(el,  // Traverse all children
            /* Look only at Element nodes */ NodeFilter.SHOW_TEXT,
            /* Filter out all but <p> */     null,
            /* Unused in HTML documents */   false);

            var text;
            while((text = walker.nextNode()) != null) {
                if(MAX_CHANGES_PER_PAGE <= 0){ return; } // We reached the max number of changes per page
                if(text.parentElement.tagName !== 'P'){ continue; }
                if(!text.textContent.toLowerCase().match((new RegExp('\\b'+e+'\\b','i')))){continue;}

                MAX_CHANGES_PER_PAGE -= 1; // So we change only 5 words per page

                lower_text = text.textContent.toLowerCase();
                var pos = lower_text.match((new RegExp('\\b'+e+'\\b','i'))).index;

                var left_side_text = document.createTextNode(lower_text.slice(0, pos));
                var right_side_text = document.createTextNode(lower_text.slice(pos+ e.length));

                var source_word = e;
                var dest_word = data[e][0];
                var word_def = data[e][1];


                var span = document.createElement('span');
                span.id = uuid();
                span.className = 'wordLearning';
                span.textContent = source_word;
                span.title = '\<span class="originalWord"\>' + dest_word + '\<\/span\>'
                    + '\<span class="definition"\>' + word_def + '\<\/span\>';

                text.parentElement.insertBefore(right_side_text, text);
                text.parentElement.insertBefore(span, right_side_text);
                text.parentElement.insertBefore(left_side_text, span);
                text.parentElement.removeChild(text);

            }
        }
    }
}

(function bootstrap(){
    chrome.extension.sendRequest({method: "getLocalStorage", key: "word_pack"}, function(response) {
        if(response.data === 'false'){ console.log('BREAK'); return; }
        var url = top.location.href;
        for(var i=0; i<BLACK_LIST.length; i++){
            if(url.indexOf(BLACK_LIST[i]) !== -1){
                return;
            }
        }
        $('span.wordLearning').tipsy({live: true, offset: 10, html: true, opacity:1});

        chrome.extension.sendRequest({method: "getLocalStorage", key: "token"}, function(response){
            authToken = response.data;
            replaceText(document.body);
        });
    });
})();